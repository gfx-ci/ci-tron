from .snmp import SnmpPDU
from .. import PDUState


class SnmpPoePDU(SnmpPDU):
    outlet_labels = '1.3.6.1.2.1.105.1.1.1.9.1'
    outlet_status = '1.3.6.1.2.1.105.1.1.1.3.1'

    state_mapping = {
        PDUState.ON: 1,
        PDUState.OFF: 2,
        PDUState.REBOOT: 3,
    }

    @property
    def default_min_off_time(self):
        return 10.0


class SnmpPoeTpLinkPDU(SnmpPDU):
    # MiB: https://mibbrowser.online/mibdb_search.php?mib=TPLINK-POWER-OVER-ETHERNET-MIB
    outlet_labels = '1.3.6.1.4.1.11863.6.56.1.1.2.1.1.1'
    outlet_status = '1.3.6.1.4.1.11863.6.56.1.1.2.1.1.2'

    state_mapping = {
        PDUState.ON: 1,
        PDUState.OFF: 0,
        PDUState.REBOOT: 2,
    }

    @property
    def default_min_off_time(self):
        return 10.0
