from .snmp import SnmpPDU
from .. import PDUState


class ApcMasterswitchPDU(SnmpPDU):
    outlet_labels = '1.3.6.1.4.1.318.1.1.4.4.2.1.4'
    outlet_status = '1.3.6.1.4.1.318.1.1.4.4.2.1.3'

    state_mapping = {
        PDUState.ON: 1,
        PDUState.OFF: 2,
        PDUState.REBOOT: 3,
    }
