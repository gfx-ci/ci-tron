# CI-tron Infrastructure

<img src="/logo/ci-tron-128px.png" alt="CI-tron, logo of the project" align="left" style="margin-right: 20px"/>

This repository contains the source for the CI-tron infrastructure. Its main
purpose is to build a multi-service container that acts as a bare-metal CI
gateway service, responsible for the orchestration and management of devices
under test, or DUTs as we call them.

Take a look to our documentation at
[https://gfx-ci.pages.freedesktop.org/ci-tron/](https://gfx-ci.pages.freedesktop.org/ci-tron/)

