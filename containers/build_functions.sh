# Parameters:
#   CI_REGISTRY (required): Hostname of the registry (eg: localhost:8088 or registry.freedesktop.org)
#   CI_PROJECT_PATH (required): Path to the project in the registry specified by $CI_REGISTRY.
#   CI_JOB_TOKEN: Token to log in to $CI_REGISTRY for the username `gitlab-ci-token`. No authentication done when missing
#   FDO_REPO_SUFFIX (required): Name of the image
#   FDO_DISTRIBUTION_PLATFORMS: Space-separated list of platforms we should generate an image for. When missing, the
#                               container image will be generated for the same platform as the gitlab runner executing
#                               it. Usual values: linux/amd64, linux/arm64/v8, linux/riscv64, or linux/arm/v6.
#   FDO_DISTRIBUTION_TAG (required): Tag to use when creating the image
#   FDO_DISTRIBUTION_TAG_LATEST: Tag to use to represent the latest generated image. No tagging if missing.
#   FDO_UPSTREAM_REPO: Project path of the upstream repository. Used to copy images from if missing in the fork.

# The final image(s) name will be:
# * `$CI_REGISTRY/$CI_PROJECT_PATH/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG`
# * `$CI_REGISTRY/$CI_PROJECT_PATH/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG_LATEST` (if FDO_DISTRIBUTION_TAG_LATEST is present)

buildah_run="buildah run --isolation chroot"
buildah_commit="buildah commit --format docker"
build_container="no"

# Work around regression in buildah 1.39
export BUILDAH_NOPIVOT=1

is_local_registry() {
	if [[ $1 =~ ^localhost.* ]]; then
		return 0
    else
		return 1
	fi
}

push_image() {
	[ -n "$CI_JOB_TOKEN" ] && [ -n "$CI_REGISTRY" ] && podman login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
	extra_podman_args='--compression-format zstd'
	is_local_registry "$IMAGE_NAME" && extra_podman_args='--tls-verify=false'

	podman image tree $IMAGE_NAME

	if [ -z ${FDO_DISTRIBUTION_PLATFORMS+x} ]; then
		cmd="podman push $extra_podman_args $IMAGE_NAME"
	else
		cmd="podman manifest push $extra_podman_args $IMAGE_NAME $IMAGE_NAME"
	fi

	$cmd || true
	sleep 2
	$cmd
}

skopeo_copy() {
	[ -n "$CI_JOB_TOKEN" ] && [ -n "$CI_REGISTRY" ] && skopeo login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY

	extra_skopeo_args=
	is_local_registry "$1" && extra_skopeo_args="$extra_skopeo_args --src-tls-verify=false"
	is_local_registry "$2" && extra_skopeo_args="$extra_skopeo_args --dest-tls-verify=false"

	skopeo copy --multi-arch all $extra_skopeo_args "docker://$1" "docker://$2"
}

tag_latest_when_applicable() {
	if [ -n "${FDO_DISTRIBUTION_TAG_LATEST:-}" ]; then
		IMAGE_NAME_LATEST="$CI_REGISTRY/$CI_PROJECT_PATH/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG_LATEST"
		skopeo_copy "$IMAGE_NAME" "$IMAGE_NAME_LATEST"
	fi
}

__skopeo_inspect() {
	skopeomsg=`skopeo inspect --tls-verify=false docker://$1  2>&1 || true`

	if [[ $skopeomsg == *"manifest unknown"* ]]; then
		skopeo_image_present="no"
	elif [[ $skopeomsg == *"Digest"* ]] &&  [[ $skopeomsg == *"Layers"* ]]; then
		skopeo_image_present="yes"
	else
		skopeo_image_present="network_problem"
	fi
}

skopeo_inspect() {
	__skopeo_inspect "$1"
	if [[ $skopeo_image_present == "network_problem" ]]; then
		echo "There was a network problem, second try in 5 seconds..."
		sleep 5
		__skopeo_inspect "$1"
		if [[ $skopeo_image_present == "network_problem" ]]; then
			echo "The network problem is still present, third and last try in 10 seconds..."
			sleep 10
			__skopeo_inspect "$1"
		fi
	fi
}

check_if_build_needed_or_crash_on_network_error() {
	# when running locally (where $CI is unset)
	if is_local_registry "$IMAGE_NAME"; then
		return 0
	fi

	skopeo_inspect "$IMAGE_NAME"
	if [[ $skopeo_image_present == "network_problem" ]]; then
		# Fail the job, we don't know for sure whether the image is missing
		exit 1
	elif [[ $skopeo_image_present == "no" ]]; then
		return 0
	else
		return 1
	fi
}

copy_upstream_image_if_present() {
	if [ -n "$FDO_UPSTREAM_REPO" ] && [[ "$FDO_UPSTREAM_REPO" != "$CI_PROJECT_PATH" ]]; then
		UPSTREAM_IMAGE_NAME="$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG"

		skopeo_inspect "$UPSTREAM_IMAGE_NAME"
		if [[ $skopeo_image_present == "yes" ]]; then
			echo "Copying the container image from the upstream repo..."

			skopeo_copy "$UPSTREAM_IMAGE_NAME" "$IMAGE_NAME"

			return 0
		fi
	fi

	return 1
}

# This function requires a "build()" function to be defined by the caller ahead of calling this function
build_and_push_single_arch_container() {
	echo "Building container ..."
	EXTRA_BUILDAH_FROM_ARGS='' build

	# Push it to the container registry
	$buildah_commit $buildcntr $IMAGE_NAME
	push_image

	# Remove the local version of the container
	buildah unmount $buildcntr
	buildah rm $buildcntr
}

# This function requires a "build()" function to be defined by the caller ahead of calling this function
build_and_push_multi_arch_container() {
	local IMAGES=""
	for platform in ${FDO_DISTRIBUTION_PLATFORMS}; do
		echo -e "\n\nBuilding $platform container ..."
		EXTRA_BUILDAH_FROM_ARGS="--platform $platform" build

		ARCH_IMAGE_NAME=$IMAGE_NAME-${platform//\//-}
		$buildah_commit $buildcntr $ARCH_IMAGE_NAME
		IMAGES="$IMAGES $ARCH_IMAGE_NAME"

		buildah unmount $buildcntr
		buildah rm $buildcntr
	done

	# Create the manifest with all the images
	buildah manifest rm ${IMAGE_NAME} || /bin/true
	buildah manifest create ${IMAGE_NAME} ${IMAGES}

	# Push it to the container registry
	push_image

	# Remove the temporary images
	podman rmi -f ${IMAGES}
}

# This function requires a "build()" function to be defined by the caller ahead of calling this function
build_and_push_container() {
	IMAGE_NAME="$CI_REGISTRY/$CI_PROJECT_PATH/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG"

	if check_if_build_needed_or_crash_on_network_error; then
		if ! copy_upstream_image_if_present; then
			echo "Couldn't find the container image in the upstream repo. Build it!"

			if [ -z ${FDO_DISTRIBUTION_PLATFORMS+x} ]; then
				build_and_push_single_arch_container
			else
				build_and_push_multi_arch_container
			fi
		fi
	fi

	# Make sure to tag the current container as latest if asked for it
	tag_latest_when_applicable
}
